# Selemca Mental world #

The software consists of two parts: server software that holds and stores the belief system and epistemics model.
And a client program that controls the server.
The server is meant to run in a server environment. It has to have access to a MySql server, a Tomcat installation
and requires a fixed IP or DNS name.
The client is meant to run on the personal computer (or laptop) of the user.

## Server installation ##

### Requirements: ###

* MySQL version 5.x. plus administrator rights to create a schema and a user
* JDK 8 or higher
* Apache tomcat 8.x

### Installation process: ###

Intended audience: professional server maintainer. You need access to a server having Java, MySQL and Tomcat installed, and have administrator rights

** MySQL **

1. create a database schema called 'beliefsystem' with character set: utf8
2. create a user 'selemca' with a password that has full access to this schema

** Configuration **

1. Make a directory to hold the software's settings file
2. Make an environment variable 'SELEMCA_HOME' pointing to the directory you created.
Persist the variable so it is set with every run of Tomcat
Alternatively create the directory /usr/local/selemca which will be used when SELEMCA_HOME is not set.
3. From the installation files copy database.properties and ServerApplicationSettings.properties to $SELEMCA_HOME
It is important that the user running tomcat has read and write access to these files! If needed hand over file ownership: chown tomcat8 *
4. Edit database.properties, set the password you used for the selemca user in MySQL

** Tomcat **

1. From the installation files copy MentalWorld.war, BeliefSystem.war and BeliefSystemAdmin.war to the webapp directory
2. Start Tomcat if not already running

** Sample data**

Add some sample data to the Belief system. The epistemics model is designed to extend on prior knowledge.
It has to have some knowledge, otherwise it is mentally blind (as in it cannot compare what it sees to anything it knows. It needs some basic context)
This sample data is just a starting point. One can later change concepts and relations, or upload a whole new belief system to replace this one.
1. Open a browser and go to: http://<server>:8080/BeliefSystemAdmin/
2. Select tab 'Import/Export'
3. Choose file 'beliefsystem.zip' from the installation files
4. Click 'Start upload'
5. The 'Concepts' and 'Associations' tabs should show the sample data.

## Client installation ##

### Requirements: ###

JRE (or JDK) 8.x

### Installation process: ###

Intended audience: average user
From the installation files copy EpistemicsClient.jar, ClientApplicationSettings.properties and wordnet_2_0.rdf files to a directory of your choice. Place them together in the same directory. We will refer to this directory as SELEMCA_HOME.
Edit ClientApplicationSettings.properties, the text 'localhost' occurs 2 times. Change these to the name of the server.
Start Epistemics client
1. From a file browser (Finder in OS X or Explorer in Windows) double-click
EpistemicsClient.jar or
1. Open a prompt
2. cd to the directory holding EpistemicsClient.jar
3. enter command: java -jar EpistemicsClient.jar